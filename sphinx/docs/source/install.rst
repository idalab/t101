============
Installation
============

At the command line::

    easy_install 

Or, if you have virtualenvwrapper installed:

.. code-block:: bash

    mkvirtualenv crawler
    pip install crawler


.. _proof-of-concept: https://en.wikipedia.org/wiki/Proof_of_concept

A proof-of-concept_ is a simplified version, a prototype of what is expected to agree on the main
lines of expected changes. `PoC <proof-of-concept_>`_ is a common abbreviation.
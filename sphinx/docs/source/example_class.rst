====================================
Documentation of Example Class 
====================================

    .. py:currentmodule:: my_code.ExampleClass

    .. autoclass:: ExampleClass
        :members:
